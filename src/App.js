import UserTable from "./components/usersTable";

function App() {
  return (
    <div>
      <UserTable/>
    </div>
  );
}

export default App;
