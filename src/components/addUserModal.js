import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { CreateNewUSerHandler, setStatus, changeStatusAddNewUser } from '../actions/users.action';
import { Grid, TextField, InputLabel, Select, MenuItem, Button, Box, Typography, Modal} from '@mui/material';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

export default function AddUserModal() {
    const {status} = useSelector((reduxData)=> reduxData.userReducer);
    const dispatch = useDispatch();
    const [newUser, getNewUser] = useState({
      firstname: "",
      lastname: "",
      subject:"",
      country:"",
      customerType: "Standard",
      registerStatus: "New",
    })
    const handleClose = () => {
        dispatch(setStatus(false))
    }
    const handleChange = (event) => {
        const value = event.target.value;
        getNewUser({
          ...newUser,
          [event.target.name] : value
        })

      };
    const onBtnCreateUserHandler = ()=>{
      console.log(newUser)
      dispatch(CreateNewUSerHandler(newUser))
      dispatch(changeStatusAddNewUser(false));
      //done
    }
  return (
    <div>
      <Modal
        open={status}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Add new User
          </Typography>
          <Typography id="modal-modal-description" sx={{ mt: 2 }}>
            <Grid container spacing={2}>
                <Grid item xs={3}>
                    <p>First Name</p>
                </Grid>
                <Grid item xs={9}>
                    <TextField onChange={handleChange} name="firstname" label="firstname" variant="outlined"fullWidth />
                </Grid>
                <Grid item xs={3}>
                    <p>Last Name</p>
                </Grid>
                <Grid item xs={9}>
                    <TextField name="lastname" onChange={handleChange} label="lastname" variant="outlined" fullWidth/>
                </Grid>
                <Grid item xs={3}>
                    <p>Subject</p>
                </Grid>
                <Grid item xs={9}> 
                    <TextField name="subject" onChange={handleChange} label="subject" variant="outlined" fullWidth/>
                </Grid>
                <Grid item xs={3}>
                    <p>Country</p>
                </Grid>
                <Grid item xs={9}>
                <InputLabel>Select Country</InputLabel>
                    <Select
                    onChange={handleChange}
                    fullWidth
                    name="country"
                    value={newUser.country}
                    >
                      <MenuItem value="VN">Việt Nam</MenuItem>
                      <MenuItem value="USA">USA</MenuItem>
                      <MenuItem value="AUS">Australia</MenuItem>
                      <MenuItem value="CAN">Canada</MenuItem>
                    </Select>
                </Grid>
            </Grid>
          </Typography>
          <Grid container mt={2} spacing={2}
                direction="row"
                justifyContent="flex-end"
                alignItems="center"> 
          <Grid item>
            <Button  onClick={handleClose} variant="outlined">Close</Button>
          </Grid>
          <Grid item>
            <Button onClick={onBtnCreateUserHandler} variant="contained">Create New User</Button>
          </Grid>
            
          </Grid>
        </Box>
      </Modal>
    </div>
  );
}