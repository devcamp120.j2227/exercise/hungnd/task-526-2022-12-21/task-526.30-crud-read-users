import { Table, TableBody, TableContainer, Grid, TableRow, TableCell, TableHead, Container, Paper, styled, Button,
    CircularProgress, TableFooter, TablePagination } from "@mui/material"
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { changeStatusDeleteUSer, changeStatusEditUser, fetchUser, getUser, setStatus } from "../actions/users.action";
import AddUserModal from "./addUserModal";
import DeleteUserModal from "./deleteUserModal";
import EditUserModal from "./editUserModal";
const UserTable = () =>{
    const dispatch = useDispatch();
    const {users, pending, totalUser , openEdit, status, openDelete} = useSelector((reduxUser)=>
        reduxUser.userReducer
    )
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
  
    const handleChangePage = (event, newPage) => {
      setPage(newPage);
    };
  
    const handleChangeRowsPerPage = (event) => {
      setRowsPerPage(+event.target.value);
      setPage(0);
    };

    useEffect(()=>{
        dispatch(fetchUser())
    },[]);
    const StyledTableRow = styled(TableRow)(({ theme }) => ({
        '&:nth-of-type(odd)': {
          backgroundColor: theme.palette.action.hover,
        },
        // hide last border
        '&:last-child td, &:last-child th': {
          border: 0,
        },
      }));
    
    const onBtnEditHandler= (value) =>{
        dispatch(getUser(value));
        dispatch(changeStatusEditUser(true))
        console.log("Edit clicked")
    }
    const onBtnDeleteHandler = (value)=>{
        dispatch(getUser(value));
        dispatch(changeStatusDeleteUSer(true));
        console.log("Delete clicked")
    }
    const onBtnAddUserHandler = ()=>{
        dispatch(setStatus(true))
        //done
    };
    return(
        <Container>
            <Grid container mt={5}>
                <Button variant="contained" style={{backgroundColor:"green"}} onClick={onBtnAddUserHandler}>ADD USER</Button>
                {status === true?<AddUserModal/>:null}
                {openEdit === true? <EditUserModal/> : null}
                {openDelete === true?  <DeleteUserModal/>: null}
                <Grid item md={12} sm={12} lg={12} xs={12}>
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} aria-label="simple table">
                            <TableHead>
                                <TableRow >
                                    <TableCell>Id</TableCell>
                                    <TableCell>First Name</TableCell>
                                    <TableCell>Last Name</TableCell>
                                    <TableCell>Country</TableCell>
                                    <TableCell>Subject</TableCell>
                                    <TableCell>Customer Type</TableCell>
                                    <TableCell>Register Status</TableCell>
                                    <TableCell>Action</TableCell>
                                </TableRow>
                            </TableHead>                           
                            { pending ? 
                            <TableBody>
                                <TableRow>
                                    <TableCell><CircularProgress/></TableCell>
                                </TableRow>
                            </TableBody>
                                :<TableBody>
                                    {users.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((value, index)=>{
                                        return (
                                            <StyledTableRow key={index}>
                                                <TableCell>{value.id}</TableCell>
                                                <TableCell>{value.firstname}</TableCell>
                                                <TableCell>{value.lastname}</TableCell>
                                                <TableCell>{value.country}</TableCell>
                                                <TableCell>{value.subject}</TableCell>
                                                <TableCell>{value.customerType}</TableCell>
                                                <TableCell>{value.registerStatus}</TableCell>
                                                <TableCell>
                                                    <Button variant="contained" onClick={()=> onBtnEditHandler(value)} >Edit</Button>   &nbsp;
                                                    <Button variant="contained" onClick={()=> onBtnDeleteHandler(value)} style={{backgroundColor:"red"}} >Delete</Button>
                                                </TableCell>
                                            </StyledTableRow>
                                        )
                                    })} 
                                </TableBody>}      
                            </Table>  
                        <TablePagination
                                    rowsPerPageOptions={[10, 25, 50, 100]}
                                    component="div"
                                    count={totalUser}
                                    rowsPerPage={rowsPerPage}
                                    page={page}
                                    onPageChange={handleChangePage}
                                    onRowsPerPageChange={handleChangeRowsPerPage}
                                />
                    </TableContainer>
                </Grid>
            </Grid>
        </Container>
        
    )
} 
export default UserTable;