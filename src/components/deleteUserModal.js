import { Grid, Button, Box, Typography, Modal} from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { changeStatusDeleteUSer, deleteUserHandler, setStatus } from '../actions/users.action';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};
export default function DeleteUserModal() {
    const {openDelete, user} = useSelector((reduxData)=> reduxData.userReducer);
    console.log("delete",user)
    const dispatch = useDispatch();

    const handleClose = () => {
        dispatch(changeStatusDeleteUSer(false))
    }
    const onBtnDeleteUserHandler = ()=>{
        dispatch(deleteUserHandler(user))
        dispatch(setStatus(false));
    }
  return (
    <div>
      <Modal
        open={openDelete}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Delete User
          </Typography>
          <Typography id="modal-modal-description" sx={{ mt: 2 }}>
            <Grid container spacing={2}>
                <Grid item>
                    <p>Bạn có chắc muốn xóa user có id {user.id}</p>
                </Grid>
            </Grid>
          </Typography>
          <Grid container mt={2} spacing={2}
                direction="row"
                justifyContent="flex-end"
                alignItems="center"> 
          <Grid item>
            <Button  onClick={handleClose} variant="outlined">Close</Button>
          </Grid>
          <Grid item>
            <Button onClick={onBtnDeleteUserHandler} variant="contained" style={{backgroundColor:"red"}}> Delete User</Button>
          </Grid>
            
          </Grid>
        </Box>
      </Modal>
    </div>
  );
}