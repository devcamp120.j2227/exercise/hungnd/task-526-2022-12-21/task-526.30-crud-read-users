import { 
    FETCH_PENDING_API,
    FETCH_SUCCESS_API,
    FETCH_ERROR_API,
    GET_USER,
    ADD_USER_MODAL,
    SET_STATUS,
    EDIT_USER,
    CREATE_NEW_USER,
    EDIT_USER_STATUS,
    DELETE_USER,
    DELETE_USER_STATUS
     } from "../constants/users.constants";

//chỉnh status nút delete
export const changeStatusDeleteUSer = (data)=>{
    return async (dispatch) =>{
        await dispatch({
            type: DELETE_USER_STATUS,
            data: data
        })
    }
}
//Thao tác nút xóa user
export const deleteUserHandler = (data)=>{
    return async (dispatch)=>{
        await dispatch({
            type: DELETE_USER,
            data: data
        });
        const fetchApi = async (url, body) => {
            const response = await fetch(url, body);
        
            const data = await response.json()
            return data;
        };
        //khai báo body api POST
        const requestOptions = {
            method: 'DELETE',
            redirect: 'follow',
            body: ""
        };
        //Call POST api
        const id = data.id
        fetchApi ("http://203.171.20.210:8080/crud-api/users/"+ id, requestOptions)
            .then((data) =>{
            })
            .catch((error)=>{
                console.log(error.message)
            })
    }
}

//chỉnh status của nút edit
export const changeStatusEditUser =(data)=>{
    return async (dispatch) =>{
        await dispatch({
            type: EDIT_USER_STATUS,
            data: data
        });
    }
}
//sửa user
export const editUserHandler = (data) =>{
    return async (dispatch) =>{
        await dispatch({
            type: EDIT_USER,
            data: data
        });
        const fetchApi = async (url, body) => {
            const response = await fetch(url, body);
        
            const data = await response.json()
            return data;
        };
        //khai báo body api POST
        const body = {
            method: 'PUT',
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            },
            body: JSON.stringify(data),
            redirect: 'follow'
        };
        //Call POST api

        fetchApi ("http://203.171.20.210:8080/crud-api/users/"+ data.id, body)
            .then((data) =>{
                console.log("User edited successfully", data); 
                alert(`Edit user with Id ${data.id} successfull`)
            })
            .then(()=>{
                window.location.reload();
            })
            .catch((error)=>{
                console.log(error.message)
            })
    }
}
//lấy thông tin user khi bấm nút sửa hoặc xóa
export const getUser = (data) =>{
    return async(dispatch)=>{
        await dispatch({
            type: GET_USER,
            data: data
        });
    }
}
//hàm truyền dữ liệu từ input modal vào body và call api POST
export const addUser = (data) =>{
    return async(dispatch)=>{
        await dispatch({
            type: ADD_USER_MODAL,
            data: data
        });
    }
}
export const changeStatusAddNewUser = (data) =>{
    return{
        type:CREATE_NEW_USER,
        data: data
    }
}
export const CreateNewUSerHandler = (value) =>{
    return async (dispatch) =>{
        await dispatch({
            type: CREATE_NEW_USER,
            data: value
        });
        const fetchApi = async (url, body) => {
            const response = await fetch(url, body);
        
            const data = await response.json()
            return data;
        };
        //khai báo body api POST
        const body = {
            method: 'POST',
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            },
            body: JSON.stringify(value),
            redirect: 'follow'
        };
        //Call POST api
        fetchApi ("http://203.171.20.210:8080/crud-api/users/", body)
            .then((data) =>{
                console.log("Create new user successfully", data);
                alert("Create new user successfully");
            })
            .then(()=>{
                window.location.reload();
            })
            .catch((error)=>{
                console.log(error.message)
            })
    }
}
//set status when click on modal add new user
export const setStatus = (data)=>{
    return{
        type: SET_STATUS,
        data: data
    }
}
//call api get all user for loading on table
export const fetchUser = () =>{
    return async(dispatch) =>{
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
          };
          await dispatch({
            type: FETCH_PENDING_API
          });
          try {
            const response =  await fetch("http://203.171.20.210:8080/crud-api/users/", requestOptions);
            const data = await response.json();
            return dispatch({
                type: FETCH_SUCCESS_API,
                data: data
            })
            
          } catch (error) {
            return dispatch({
                type: FETCH_ERROR_API,
                error: error
            })
          }
    }
}